package com.exam.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;


@SpringBootApplication
@EnableEurekaClient 		// It acts as a eureka client
public class SpringExamGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringExamGatewayApplication.class, args);
	}

}
