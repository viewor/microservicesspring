package com.exam.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class SpringExamEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringExamEurekaApplication.class, args);
	}

}
