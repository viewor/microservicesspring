package com.exam.usuario.dao.entity;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.exam.usuario.dao.entity.common.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import lombok.Data;

@Data
@Document(collection = "usuario")
public class Usuario extends BaseEntity{
	
	@ApiModelProperty(value="Nombre completo", required=true)
	private String nombreCompleto;
	
	@ApiModelProperty(value="Contraseña", required=true)
	private String password;
	
	@ApiModelProperty(value="Ultimos passwords", required=false, hidden=true, accessMode=AccessMode.READ_ONLY)
	private String[] ultimosPassword;
	
	@ApiModelProperty(value="Correo electronico, no se puede repetir", required=true)
	@Indexed(unique = true)
	private String email;
	
	@ApiModelProperty(value="Direccion de la persona", required=true)
	private String direccion;
	
	@ApiModelProperty(value="Nombre de usuario, no se puede repetir", required=true)
	@Indexed(unique = true)
	private String nickname;

}
