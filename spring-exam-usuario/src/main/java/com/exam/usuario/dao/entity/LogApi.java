package com.exam.usuario.dao.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import io.swagger.annotations.ApiModelProperty;
import com.exam.usuario.dao.entity.common.BaseEntity;
import lombok.Data;

@Data
@Document(collection = "log")
public class LogApi extends BaseEntity{
	public class Acciones{
		public static final String NUEVO_USUARIO = "NUEVO_USUARIO";
		public static final String FIND_ALL = "FIND_ALL";
		public static final String FIND_OID = "FIND_OID";
		public static final String FIND_EMAIL = "FIND_EMAIL";
		public static final String FIND_NICKNAME = "FIND_NICKNAME";
		public static final String PATCH_USUARIO = "PATCH_USUARIO";
		public static final String INICIO_SESION = "INICIO_SESION";
		public static final String DELETE_USUARIO = "DELETE_USUARIO";
		public static final String UPDATE_USUARIO = "UPDATE_USUARIO";
	}
	
	@ApiModelProperty(value="Accion realizada", required=true)
	private String accion;
	
	@ApiModelProperty(value="Usuario accion", required=true)
	private String usuario;
	
	@ApiModelProperty(value="Token access", required=false)
	private String token;
	
	@ApiModelProperty(value="Informacion", required=false)
	private String informacion;
}
