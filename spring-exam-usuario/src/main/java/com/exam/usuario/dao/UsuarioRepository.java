package com.exam.usuario.dao;
import com.exam.usuario.dao.entity.Usuario;

import reactor.core.publisher.Mono;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends ReactiveMongoRepository<Usuario, String> {
	
	Mono<Usuario> findByEmail(String email);
	
	Mono<Usuario> findByNickname(String nickname);

}
