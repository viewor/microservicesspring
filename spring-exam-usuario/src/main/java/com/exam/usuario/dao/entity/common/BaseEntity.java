package com.exam.usuario.dao.entity.common;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.*;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;

@Setter
@Getter
public abstract class BaseEntity implements Serializable  {

	@ApiModelProperty(value="Identificador unico", required=false, hidden=true, accessMode=AccessMode.READ_ONLY)
	@Id
    private String id;
	

	@ApiModelProperty(value="Fecha de creacion", required=false, hidden=true,  accessMode=AccessMode.READ_ONLY)
    @CreatedDate
    private Date createdDate;
}
