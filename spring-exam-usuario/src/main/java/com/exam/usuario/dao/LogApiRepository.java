package com.exam.usuario.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.exam.usuario.dao.entity.LogApi;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface LogApiRepository extends ReactiveMongoRepository<LogApi, String> {

	Flux<LogApi> findByAccion(String accion);
	
	Flux<LogApi> findByUsuario(String usuario);
	
	Mono<Boolean> deleteByUsuario(String usuario);
	
}
