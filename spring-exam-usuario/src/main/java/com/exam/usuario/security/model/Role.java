package com.exam.usuario.security.model;

public enum Role {
	ROLE_USER, ROLE_ADMIN
}