package com.exam.usuario.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;

@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class WebSecurityConfig {

	@Autowired
	private ReactiveAuthenticationManager authenticationManager;
	
	@Autowired
	private ServerSecurityContextRepository serverSecurityContextRepository;

	@Bean
	public SecurityWebFilterChain
	securitygWebFilterChain(ServerHttpSecurity http) {
		return http.csrf().disable()
				.formLogin().disable()
				.httpBasic().disable()
				.logout().disable()
				.authenticationManager(authenticationManager)
				.securityContextRepository(serverSecurityContextRepository)
				.authorizeExchange()
				.pathMatchers(HttpMethod.POST, "/api/usuario/login").permitAll()
				.pathMatchers(HttpMethod.PATCH).authenticated()
				.pathMatchers(HttpMethod.DELETE).authenticated()
				.pathMatchers(HttpMethod.PUT).authenticated()
				.pathMatchers(HttpMethod.GET).permitAll()
				//.anyExchange().permitAll()
				.and().build();
		
	}
	
}