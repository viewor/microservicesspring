package com.exam.usuario.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.exam.usuario.dao.entity.Usuario;
import com.exam.usuario.security.JWTUtil;
import com.exam.usuario.security.PBKDF2Encoder;
import com.exam.usuario.security.model.AuthRequest;
import com.exam.usuario.security.model.AuthResponse;
import com.exam.usuario.service.UsuarioService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Api
@Slf4j
@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {
	@Autowired
    private UsuarioService usuarioService;
	
	@Autowired
	private PBKDF2Encoder passwordEncoder;
	
	@Autowired
	private JWTUtil jwtUtil;
	
	@PostMapping("/login")
	public  Mono<ResponseEntity<? extends Object>> login(Authentication authentication, @ApiParam(name = "Usuario", value = "Login", required = true) @RequestBody AuthRequest ar){
		return usuarioService.findByNickname(ar.getUsername(), authentication).map((userDetails) -> {
			if (passwordEncoder.encode(ar.getPassword()).equals(userDetails.getPassword())) {
				return ResponseEntity.ok(new AuthResponse(jwtUtil.generateToken(userDetails)));
			} else {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
			}
		}).defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@GetMapping
    public Flux<Usuario> findAll(Authentication authentication) {
        log.debug("GetAll Usuario");
        return usuarioService.findAll(authentication);
    }
	

	@GetMapping("/{oid}")
    public Mono<ResponseEntity<Usuario>> findByOID(Authentication authentication, @PathVariable String oid) {
        log.debug("GetByOID Usuario with oid : {}", oid);
        return usuarioService.findByOID(oid, authentication)
        		.map(findUsuario -> ResponseEntity.ok(findUsuario))
        		.defaultIfEmpty(ResponseEntity.notFound().build());
    }
	
	@GetMapping("/findByEmail/{email}")
    public Mono<ResponseEntity<Usuario>> findByEmail(Authentication authentication, @PathVariable String email) {
        log.debug("GetByEmail Usuario with email : {}", email);
        return usuarioService.findByEmail(email, authentication)
        		.map(findUsuario -> ResponseEntity.ok(findUsuario))
        		.defaultIfEmpty(ResponseEntity.notFound().build());
    }
	
	@GetMapping("/findByNickname/{nickname}")
    public Mono<ResponseEntity<Usuario>> findByNickname(Authentication authentication, @PathVariable String nickname) {
        log.debug("GetByNickname Usuario with nickname : {}", nickname);
        return usuarioService.findByNickname(nickname, authentication)
        		.map(findUsuario -> ResponseEntity.ok(findUsuario))
        		.defaultIfEmpty(ResponseEntity.notFound().build());
    }
	
	@PatchMapping("/{oid}")
    public Mono<ResponseEntity<Usuario>> patchUsuario(Authentication authentication, @PathVariable String oid, @RequestBody Map<String, Object> data) {
        log.debug("Patch update : {}", data);
        return usuarioService.partialUpdateUsuario(data, oid, authentication.getName())
        		.map(updateUsuario -> ResponseEntity.ok(updateUsuario))
        		.defaultIfEmpty(ResponseEntity.notFound().build());
    }
	
	
	@PostMapping
	public Mono<ResponseEntity<Usuario>> createUsuario(Authentication authentication, @ApiParam(name = "Usuario", value = "Usuario a crear", required = true) @RequestBody Usuario usuario){
		usuario.setPassword( passwordEncoder.encode(usuario.getPassword()));
		if (authentication != null)
			return usuarioService.createUsuario(usuario, authentication.getName())
				.map(createUsuario -> ResponseEntity.status(HttpStatus.CREATED).body(createUsuario))
	    		.defaultIfEmpty(ResponseEntity.notFound().build());
		else
			return usuarioService.createUsuario(usuario)
					.map(createUsuario -> ResponseEntity.status(HttpStatus.CREATED).body(createUsuario))
		    		.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@DeleteMapping("/{oid}")
	public Mono<ResponseEntity<Boolean>> deleteUsuario(Authentication authentication, @PathVariable String oid){
		return usuarioService.delete(oid, authentication.getName())
				.map(isDelete -> ResponseEntity.ok(isDelete))
        		.defaultIfEmpty(ResponseEntity.notFound().build());
	}
}
