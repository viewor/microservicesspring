package com.exam.usuario.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.usuario.dao.LogApiRepository;
import com.exam.usuario.dao.entity.LogApi;
import com.exam.usuario.service.LogApiService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class LogApiServiceImpl  implements LogApiService{

	@Autowired
	private LogApiRepository logApiRepository;
	
	@Override
	public Mono<LogApi> createLog(LogApi logApi) {
		return logApiRepository.insert(logApi);
	}

	@Override
	public Mono<Boolean> deleteLog(String usuario) {
		return logApiRepository.deleteByUsuario(usuario);
	}

	@Override
	public Flux<LogApi> findByUsuario(String usuario) {
		return logApiRepository.findByUsuario(usuario);
	}

	@Override
	public Flux<LogApi> findByAccion(String accion) {
		return logApiRepository.findByAccion(accion);
	}
	
}
