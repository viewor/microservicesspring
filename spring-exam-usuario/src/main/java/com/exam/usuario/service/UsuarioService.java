package com.exam.usuario.service;

import java.util.Map;

import org.springframework.security.core.Authentication;

import com.exam.usuario.dao.entity.Usuario;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UsuarioService {

    Mono<Usuario> createUsuario(Usuario usuario, String username);
    Mono<Usuario> createUsuario(Usuario usuario);

    Mono<Usuario> updateUsuario(Usuario usuario, String id, String username);
    
    Mono<Usuario> partialUpdateUsuario(Map<String, Object> updates, String id, String username);

    Flux<Usuario> findAll(Authentication auth);

    Mono<Usuario> findByOID(String id, Authentication auth);

    Mono<Usuario> findByNickname(String nickname, Authentication auth);
    
    Mono<Usuario> findByEmail(String email, Authentication auth);
    
    Mono<Boolean> delete(String id, String username);

}
