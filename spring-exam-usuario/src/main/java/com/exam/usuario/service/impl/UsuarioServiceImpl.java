package com.exam.usuario.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.exam.usuario.dao.UsuarioRepository;
import com.exam.usuario.dao.entity.LogApi;
import com.exam.usuario.dao.entity.Usuario;
import com.exam.usuario.security.PBKDF2Encoder;
import com.exam.usuario.service.UsuarioService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UsuarioServiceImpl implements UsuarioService{
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
    private LogApiServiceImpl logApiServiceImpl;
	
	@Autowired
	private PBKDF2Encoder passwordEncoder;
	
	private Boolean isValidAuth(Authentication auth) {
		if (auth != null && auth.isAuthenticated())
			return true;
		return false;
	}

	private void createUsuarioLog(String info, String accion, String username) {
		usuarioRepository.findByNickname(username).doOnSuccess(currentUsuario -> {
			LogApi logApi = new LogApi();
			logApi.setAccion(accion);
			logApi.setUsuario(currentUsuario.getId());
			logApi.setCreatedDate(new Date());
			logApi.setInformacion(accion +" : " + info);
			logApiServiceImpl.createLog(logApi).subscribe();
		}).subscribe();
	}

	private void createUsuarioLog(Usuario usuario, String accion, String username) {
		usuarioRepository.findByNickname(username).doOnSuccess(currentUsuario -> {
			LogApi logApi = new LogApi();
			logApi.setAccion(accion);
			logApi.setUsuario(currentUsuario.getId());
			logApi.setCreatedDate(new Date());
			logApi.setInformacion(accion +" : " + usuario.getId());
			logApiServiceImpl.createLog(logApi).subscribe();
		}).subscribe();
	}
	
	private void createUsuarioLog(Usuario usuario, String accion, String username, String patchField) {
		usuarioRepository.findByNickname(username).doOnSuccess(currentUsuario -> {
			LogApi logApi = new LogApi();
			logApi.setAccion(accion);
			logApi.setUsuario(currentUsuario.getId());
			logApi.setCreatedDate(new Date());
			logApi.setInformacion(accion + " field "+ patchField +": " + usuario.getId());
			logApiServiceImpl.createLog(logApi).subscribe();
		}).subscribe();
	}
	
	
	@Override
	public Mono<Usuario> createUsuario(Usuario usuario, String username) {
		return usuarioRepository.insert(usuario)
				.doOnSuccess(auxUsuario -> createUsuarioLog(auxUsuario, LogApi.Acciones.NUEVO_USUARIO, username));
	}
	
	@Override
	public Mono<Usuario> createUsuario(Usuario usuario) {
		return usuarioRepository.insert(usuario);
	}

	@Override
	public Mono<Usuario> updateUsuario(Usuario usuario, String id, String username) {
		return usuarioRepository.findById(id).doOnSuccess(findUsuario -> {
			findUsuario.setNombreCompleto(usuario.getNombreCompleto());
			findUsuario.setPassword(usuario.getPassword());
			findUsuario.setEmail(usuario.getEmail());
			findUsuario.setDireccion(usuario.getDireccion());
			findUsuario.setNickname(usuario.getNickname());
			usuarioRepository.save(findUsuario).subscribe();
        }).doOnSuccess(auxUsuario -> createUsuarioLog(auxUsuario, LogApi.Acciones.UPDATE_USUARIO, username));
	}

	
	@Override
	public Mono<Usuario> partialUpdateUsuario(Map<String, Object> updates, String id, String username){
		return usuarioRepository.findById(id).doOnSuccess(findUsuario -> {
			updates.forEach((key, value) -> {
				try {
					if (key.equals("password")) {
						BeanUtils.setProperty(findUsuario, key, passwordEncoder.encode((String) value));
					}else {
						BeanUtils.setProperty(findUsuario, key, value);
					}
					usuarioRepository.save(findUsuario).subscribe();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			});
		}).doOnSuccess(auxUsuario -> createUsuarioLog(auxUsuario, LogApi.Acciones.PATCH_USUARIO, username, updates.keySet()
				.toArray()[0].toString()) );
	}

	@Override
	public Flux<Usuario> findAll(Authentication auth) {
		if (isValidAuth(auth)) {
			createUsuarioLog("findAll", LogApi.Acciones.FIND_ALL, auth.getName());
		}
		return usuarioRepository.findAll();
	}

	@Override
	public Mono<Usuario> findByOID(String id, Authentication auth) {
		if (isValidAuth(auth)) {
			createUsuarioLog(" busco: " + id , LogApi.Acciones.FIND_OID, auth.getName());
		}
		return usuarioRepository.findById(id);
	}

	@Override
	public Mono<Usuario> findByNickname(String nickname, Authentication auth) {
		if (isValidAuth(auth)) {
			createUsuarioLog(" busco: " + nickname , LogApi.Acciones.FIND_NICKNAME, auth.getName());
		}
		return usuarioRepository.findByNickname(nickname);
	}
	
	@Override
	public Mono<Usuario> findByEmail(String email, Authentication auth) {
		if (isValidAuth(auth)) {
			createUsuarioLog(" busco: " + email , LogApi.Acciones.FIND_EMAIL, auth.getName());
		}
		return usuarioRepository.findByEmail(email);
	}

	@Override
	public Mono<Boolean> delete(String id, String username) {
		return usuarioRepository.findById(id).doOnSuccess(findUsuario -> {
			usuarioRepository.delete(findUsuario).subscribe();
			logApiServiceImpl.deleteLog(id).subscribe();
		}).flatMap(findUsuario -> Mono.just(Boolean.TRUE))
				.doOnSuccess(auxUsuario -> createUsuarioLog(" elimino a: " + id, LogApi.Acciones.DELETE_USUARIO, username));
	}

}
