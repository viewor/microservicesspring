package com.exam.usuario.service;

import com.exam.usuario.dao.entity.LogApi;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface LogApiService {
	
	Mono<LogApi> createLog(LogApi logApi);
	
	Mono<Boolean> deleteLog(String usuario);
	
	Flux<LogApi> findByUsuario(String usuario);
	
	Flux<LogApi> findByAccion(String accion);
}
