package com.exam.usuario.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.builders.ResponseMessageBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMethod;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebFlux;

@Configuration
@EnableSwagger2WebFlux
@Import(springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {
	final List<ResponseMessage> globalResponses = Arrays.asList(
	        new ResponseMessageBuilder()
	            .code(201)
	            .message("Creado")
	            .build(),
            new ResponseMessageBuilder()
	            .code(401)
	            .message("No Autorizado")
	            .build(),
	        new ResponseMessageBuilder()
	            .code(409)
	            .message("Conflicto")
	            .build(),
	        new ResponseMessageBuilder()
	            .code(500)
	            .message("Error del servidor")
	            .build());

	
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)
           .ignoredParameterTypes(Authentication.class)
          .apiInfo(apiInfo()).useDefaultResponseMessages(false)
          .globalResponseMessage(RequestMethod.GET, globalResponses)
          .globalResponseMessage(RequestMethod.POST, globalResponses)
          .globalResponseMessage(RequestMethod.DELETE, globalResponses)
          .globalResponseMessage(RequestMethod.PUT, globalResponses)
          .globalResponseMessage(RequestMethod.PATCH, globalResponses)
          .enable(true)
          .securitySchemes(Arrays.asList(apiKey()))
          .securityContexts(Arrays.asList(securityContext()))
          .select()                                  
          	.paths(PathSelectors.any())                          
          	.build()
          	.securitySchemes(Arrays.asList(apiKey()));          	
    }
	
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("CRUD Usuario")
                .description("Maneja usuarios")
                .version("0.1-SNAPSHOT")
                .termsOfServiceUrl("http://viewor.mx")
                .license("Open source licensing")
                .licenseUrl("https://help.github.com/articles/open-source-licensing/")
                .build();
    }
    
    private ApiKey apiKey() {
	    return new ApiKey("JWT", "Authorization", "header");
	}
    
    
    private SecurityContext securityContext() {
        return SecurityContext.builder()
            .securityReferences(defaultAuth())
            .forPaths(PathSelectors.regex("/api.*"))
            .build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope
            = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(
            new SecurityReference("JWT", authorizationScopes));
    }
    
    @Bean
    public SecurityConfiguration security() {
        return SecurityConfigurationBuilder.builder()
                .clientId("bearer")
                .clientSecret("secret")
                .realm("realm")
                .appName("test-app")
                .scopeSeparator(",")
                .additionalQueryStringParams(null)
                .useBasicAuthenticationWithAccessCodeGrant(false)
                .build();
    }
}
