package com.exam.server.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class SpringExamServerConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringExamServerConfigApplication.class, args);
	}

}
